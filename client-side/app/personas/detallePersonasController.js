// Utilizamos esta construccion rara para evitar
// corromper el namespace global con variables 
// que solo se utilizan para esta definición

(function(angular) {

	// con esto solicitamos una referencia al modulo previamente definido
	var app = angular.module('personas');

	// con esto definimos el contorlador y asignamos funciones
	app.controller('detallePersonasCtrl',
		["$scope","$routeParams", "servicioPersonas", 
		function($scope, $routeParams,servicioPersonas){

  		 	servicioPersonas.getPersona($routeParams.personId)
	        	.then(function(data){
	            	$scope.person = data;
	        	});  
        	$scope.guardar = function(persona){

            	servicioPersonas.save(persona)
            		.then(function(data){
            			$scope.mensaje="Wuena choros";

            		});

    		};
		}]
	);

})(angular);