(function(angular){
    var app = angular.module('personas');
    app.directive('directivaPersona', function() {

        return {
            restrict: "E",
            replace: true,
            scope: {
                person: "=person"
            },
            templateUrl: 'directivaPersona.html',
            link: function($element){},
            controller: ['$scope',function($scope)  {
                $scope.detalles=false;

                $scope.verMas= function() {
                    $scope.detalles=true;
                }
                $scope.verMenos=function() {
                    $scope.detalles=false;
                }

            }]
        }
    })
})(angular);