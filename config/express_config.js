module.exports = function(app, express){

    var config = this;

    var path = require('path');

    app.configure(function(){
        app.set('port', process.env.PORT || 3000);
        app.use(express.favicon());
        app.use(express.logger('dev'));
        app.use(express.bodyParser());
        app.use(express.methodOverride());
        app.use(app.router);

        app.use(express.static(path.join(__dirname, '../public')));
    });

    console.log("serving static files from: "+path.join(__dirname, '../public'));

    //env specific config
    app.configure('development', function(){
        app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    });

    app.configure('production', function(){
        app.use(express.errorHandler());
    });

    var apiRouteConfig = require('./api_routes.js');
    apiRouteConfig.setup(app);

    var publicRouteConfig = require('./public_routes.js');
    publicRouteConfig.setup(app);

    return config;

};