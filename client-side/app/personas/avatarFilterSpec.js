describe('filtroavatar', function(){

	chai.should();

	var filter=null;
	beforeEach(module('personas'));
	beforeEach(inject(function($filter){
		filter=$filter('avatarFilter');

	}));
	it('Debe existir filtro y comprobamos que no sea NULL',function(){
		filter.should.not.be.null;
	});

	it('Transformar el input en una URL',function(){
		filter("Manuel Suarez").should.be.equal("http://placehold.it/32x32/ffff00/000000&text=MS");
	});

	it('Transformar el input en una URL 2',function(){
		filter("Manuel Ahumada").should.be.equal("http://placehold.it/32x32/ffff00/000000&text=MA");
	});
});