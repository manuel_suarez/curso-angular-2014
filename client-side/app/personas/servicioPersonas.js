(function(angular){
	var app= angular.module('personas');

	app.factory("servicioPersonas", ['$resource', function ($resource){
		// variable Global
		var personasResources = $resource('/api/people/:cod',{cod:'@cod'},
		{
			'update':{method:'PUT'}
		}); 
		
		return {
			getPersonas: function () {
			 return personasResources.query().$promise;
			},			
			getPersona:function(idPersona){
				return personasResources.get({cod:idPersona}).$promise;
			},
			save:function(persona){
				return personasResources.update({
					cod:persona.id
				},
				persona).$promise;
			}
		};
	}]);
})(angular);