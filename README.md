Ejemplos de Angular JS
==================

Ejemplos prácticos de angular js.

Estos archivos están pensados para acompañar el curso de Angular JS, y por lo tanto quizas el código no esté completamente documentado, ya que se espera una discusión en clase y entendimiento mediante ejercicios.

Ejecución
===========

Para los propósitos de esta guía, asumo que has clonado el repositorio en `~/home/code/angularjs`

Para ejecutar estos ejemplos requieres tener instalado Nodejs. Puedes conseguirlo desde http://nodejs.org para Mac, Linux o Windows.

También requieres tener instalado Grunt. Esto lo puedes instalar con la línea de comando (una vez instalado Node.js) 

	~ $ npm install grunt-cli -g


También requieres tener instalado Bower. Esto lo puedes instalar con la línea de comando (una vez instalado Node.js) 

	~ $ npm install bower -g

Para ejecutar los tests, vamos a utilizar un framework llamado Karma junto a un plugin de karma llamado PhantomJs. 

	~/home/code/angularjs $ npm install karma-cli -g
	~/home/code/angularjs $ npm install phantomjs -g


Una vez tengas instalado node.js, bower y grunt, ejecuta:

	~/home/code/angularjs $ npm install
	~/home/code/angularjs $ bower install

Esto va a bajar un montón de cosas de la internet así que ve a buscar un café mientras termina.

Una vez terminada la descarga, basta con ejecutar las siguientes lineas de comando para iniciar un servidor local:

	~/home/code/angularjs $ grunt build
	~/home/code/angularjs $ npm start

Esto levantará un servidor en el puerto 3000: http://localhost:3000

Para ejecutar pruebas unitarias, puedes ejecutar la siguiente linea de comando:
	
	~/home/code/angularjs $ npm test

Para ejecutar un servidor de desarrollo que vigile cambios de archivo y actualice el navegador, ejecuta:
	
	~/home/code/angularjs $ grunt dev

Estructura de code-dojo
============
La carpeta `api` contiene una api web que se utiliza para obtener datos por los ejemplos. Este es un código muy malo, sin pruebas y solo pensado para este ejemplo, por lo cual preferiría que ni lo miraras.

La carpeta `bower_components` contiene las librerías client-side que han sido descargadas mendiante bower. Esto incluye angularjs, bootstrap, jquery, font-awesome, entre otros.

La carpeta `client-side` tiene todos los fuentes específicos de la aplicación. Dentro de ella, la carpeta `app` contiene html y js para cada uno de los ejemplos. La carpeta less contiene los estilos en lenguaje less que hace referencia a su vez a bootstrap y font-awesome de `bower_components`. La carpeta specs contiene pruebas unitarias para cada uno de los ejemplos.

La carpeta `config` contiene parámetros y scripts de configuración para ejecutar la api, que está en nodejs y para servir los archivos de los ejemplos.

La carpeta `data` contiene archivos JSON para la api

La carpeta `node_modules` contiene librerías descargadas mediante npm.  

La carpeta `public` recibe los archivos procesados por grunt para ser emitidos mediante nodejs. Esto significa que aquí se generan los .css a partir de los .less y el js final a partir de todos los js mezclados desde `client-side` y también los html, entre otras cosas.

Node js
========

Node js es una herramienta de linea de comandos (ejecutable) que permite ejecutar javascript sin un browser de por medio, y darle acceso a toda una gama de recursos que nunca había tenido, como al sistema de archvio o comunicaciones de socket directas. Nodejs además contiene un conjunto de librerías estandar para realizar operaciones de más alto nivel como levantar un server http escuchando en un puerto y otras cosas.

Node js también contiene un administrador de paquetes y dependencias, llamado NPM, el cual te permite rápidamente descargar librerías externas para realizar trabajos de aun más alto nivel, como levantar un sitio web con el patrón de diseño MVC, siendo el más común expressjs. 

No necesitas comprender de Node.Js para entender estos ejemplos, ya que están enfocados en Angular Js y como tal, solo necesitas entender la parte cliente.

En Node.Js solo está implementada una pequeña apí HTTP JSON que permite a la aplicación Angular obtener datos de manera realista.

Internamente lo que realiza es exponer por http los métodos para crear, listar y editar "personas". Para persistencia utiliza archivos json que se ubican en la carpeta `/data` del repositorio.

Esta implementación es muy básica y no demuestra para nada prácticas recomendadas del desarrollo en Node.js, es solamente algo rápido que levanté para poder ejemplificar las comunicaciones HTTP JSON. 

Si quieres aprender sobre Node.JS te recomiendo solicitarnos un curso en donde podamos preparar código útil y consejos de cómo funcionar en este gran framework javascript. 