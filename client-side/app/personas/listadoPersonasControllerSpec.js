describe('modulo personas', function() {

  var $scope = null;
  var ctrl = null;
  var servicioPersonaFueLlamado = false;
  // habilitamos la interfaz should de chai
  chai.should();

  // antes de cada test, instanciamos el 
  // modulo 'holamundo' de nuestra aplicacion
  // angularjs. El método module() que permite esto, 
  // es provisto por el script angular-mocks.js
  // que cargamos con karma (karma.conf.js)
  beforeEach(module('personas'));


  // el método inject también es provisto por
  // angular-mocks, y nos permite injectar dependencias
  // tal como lo haría angularjs a nuestros módulos
  // pero dentro de nuestros tests
  beforeEach(inject(function($rootScope, $controller) {

    // generamos un nuevo scope para nuestro controller
    // este scope va a ser hijo de $rootScope tal cual 
    // como sería si angularjs lo hiciera.
    // Lo creamos nosotros para poder tener una referencia
    // hacia el y poder hacerle preguntas sobre su estado
    $scope = $rootScope.$new();

    // creamos el controller injectando nuestro scope
    // recién creado.
    ctrl = $controller('listadoPersonasCtrl', {
      $scope: $scope,
      servicioPersonas:{getPersonas: function () {
        servicioPersonaFueLlamado=true;
      }}
    });
  }));

//  it('$scope.people debe ser distinto de null', function() {
//
//    $scope.people.should.not.be.null;     
//    
//  });
//
//
//
//  it('$scope.people debe tener largo 6', function() {
//
//    $scope.people.length.should.be.equal(6);     
//    
//  });

  it('Verificar llamada a servicio personas desde el controlador', function() {
      servicioPersonaFueLlamado.should.be.true;
  });


});
