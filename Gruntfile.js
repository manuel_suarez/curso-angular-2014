// Generated on 2014-01-31 using generator-angular 0.6.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    //require('time-grunt')(grunt);

    
    // Define the configuration for all the tasks
    grunt.initConfig({
        config: {
            bower_root: "./bower_components",
            sourcesPath: "./client-side",
            publicPath: "./public"
        },

        less: {
            dev: {
                options: {
                    sourceMap: true,
                    sourceMapFilename: '<%= config.publicPath %>/assets/css/styles.map',
                    sourceMapURL: '/assets/css/styles.map',
                    paths: ['']
                },
                files: {
                    '<%= config.publicPath %>/assets/css/styles.css': '<%= config.sourcesPath %>/less/styles.less'
                }
            }
        },
        uglify: {
            dev: {
                options: {
                    sourceMap: true,
                    sourceMapName: '<%= config.publicPath %>/assets/js/app.min.js.map'
                },
                files: {
                    '<%= config.publicPath %>/assets/js/app.min.js': [
                                                                        '<%= config.sourcesPath %>/app/personas/*.js',
                                                                        '<%= config.sourcesPath %>/app/app.js',
                                                                        '!<%= config.sourcesPath %>/app/**/*Spec.js'
                                                                    ]
                }
            }
        },
        // Copies remaining files to places other tasks can use
        copy: {
            dev: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.bower_root %>/fontawesome/fonts',
                        dest: '<%= config.publicPath %>/assets/fonts',
                        src: [
                            '*.{otf,eot,svg,ttf,woff}'
                        ]
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.bower_root %>/angular',
                        dest: '<%= config.publicPath %>/assets/js',
                        src: 'angular.min.js'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.bower_root %>/angular-animate',
                        dest: '<%= config.publicPath %>/assets/js',
                        src: 'angular-animate.min.js'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.bower_root %>/angular-route',
                        dest: '<%= config.publicPath %>/assets/js',
                        src: 'angular-route.min.js'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.bower_root %>/angular-resource',
                        dest: '<%= config.publicPath %>/assets/js',
                        src: 'angular-resource.min.js'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.sourcesPath %>/app',
                        dest: '<%= config.publicPath %>/',
                        src: '**/*.html'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.sourcesPath %>/app',
                        dest: '<%= config.publicPath %>/',
                        src: '**/*.json'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= config.sourcesPath %>/img',
                        dest: '<%= config.publicPath %>/',
                        src: '**/*.gif'
                    }
                ]
            }
        },
         watch: {
            js: {
                files: ['<%= config.sourcesPath %>/app/**/*.js'],
                tasks: ['uglify:dev','copy:dev'],
                options: {
                    livereload:true
                }
            },
            html: {
                files: ['<%= config.sourcesPath %>/app/**/*.html'],
                tasks: ['copy:dev'],
                options: {
                    livereload:true
                }
            },
            jsTest: {
                files: ['<%= config.sourcesPath %>/spec/**/*.js']
            },
            less: {
                files: ['<%= config.sourcesPath %>/less/**/*.less'],
                tasks: ['less:dev'],
                options: {
                    livereload:true
                }
            },

            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: {
                        port: 3001,
                        // Change this to '0.0.0.0' to access the server from outside.
                        hostname: 'localhost',
                        livereload: 35729
                    }
                },
                files: [
                    '<%= config.publicPath %>/**/*.html',
                    '<%= config.publicPath %>/**/*.css',
                    '<%= config.publicPath %>/**/*.js'
                ]
            }
        }
    });


    grunt.registerTask('build', [
        'copy:dev',
        'uglify:dev',
        'less:dev'
    ]);
    
    grunt.registerTask('dev', [
        'copy:dev',
        'uglify:dev',
        'less:dev',
        'watch'
    ]);
};
