var fs = require('fs');

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function wait(seconds) {
    var date = new Date();
    var actualSeconds =  getRandomInt(0,seconds);
    var targetDate = new Date(date.getTime() + (1000 * actualSeconds));
    while (true){
        if((new Date())>= targetDate)
            return;
    }
}

var _baseDir = "data/";
function _readFileAsJson(filePath) {
    var data = fs.readFileSync(filePath, 'utf8')

    data = JSON.parse(data);
    return data;

}

function readDataFile(documentName, recordId) {
    return _readFileAsJson(_baseDir+documentName+"/"+recordId+".json");
}

function saveSingle(documentName, data) {
    var content =JSON.stringify(data);
    var filePath=_baseDir+documentName+"/"+data.id+'.json'
    fs.unlinkSync(filePath);
    fs.writeFile(filePath, content, function (err) {
        if (err) throw err;
    });
}

function deleteSingle(documentName, id) {
    var filePath=_baseDir+documentName+"/"+id+'.json'
    fs.unlinkSync(filePath);
}

function createSingle(documentName, data){

    var files = fs.readdirSync(_baseDir+documentName);

    var c=0;
    var maxId=0;
    files.forEach(function(file){
        c++;
        var strId= file.toString().replace(/.*(\d+)\.json/,"$1")
        var id = parseInt(strId);
        if(id>maxId)
            maxId = id;
    });

    maxId++;
    var filePath=_baseDir+documentName+"/"+maxId+'.json'
    data.id=maxId;
    var content =JSON.stringify(data);
    fs.writeFileSync(filePath, content);
    return maxId;
}

function readAllDataFiles(documentName){
    var collection = new Array();
    var files = fs.readdirSync(_baseDir+documentName);

    var c=0;
    files.forEach(function(file){
        c++;
        var fileName =_baseDir+documentName+"/"+file;
        var json = _readFileAsJson(fileName);
        collection.push(json);
    });

    return collection;
}


exports.getPerson = function(id) {
    wait(5);
    return readDataFile("person",id);
}

exports.savePerson = function(object) {
    wait(5);
    saveSingle('person',object);
}

exports.deletePerson = function(id) {
    wait(5);
    deleteSingle('person',id);
}

exports.getPeople = function() {
    wait(5);
    return readAllDataFiles("person");
}
exports.createPerson=function(object) {
    wait(5);
    var id = createSingle("person",object)
    return id;
}
